import React, { useState } from "react";
import { ChangeTask } from "./ChangeTask";
import { useRef } from "react";

interface TaskProps {
  key: string
  number: number
  text: string
  index: number
}

export const Task = (props: TaskProps) => {
  const [isOpenList, setIsOpenList] = useState(false)
  const [isInput, setIsInput] = useState(false)
  const [value, setValue] = useState(props.text)

  let textInput = useRef<HTMLInputElement>(null)


  function changeValue(event: any) {
    event.preventDefault()
    if (textInput.current?.value) {
      setValue(textInput.current?.value)
    }
    if (textInput.current?.value === '') {
      setValue('')
    }
  }

  function changeListOpen() {
    setIsOpenList(!isOpenList)
  }

  if (isInput) {
    console.log('пывпаы')
  }


  return (
    <>
      < li key={props.key} className="LeftContainer__tasks-item">
        <div className="LeftContainer__tasks-info" id={props.key}>
          <span className="LeftContainer__tasks-number">
            <p className="LeftContainer__tasks-number">{props.number}</p>
          </span>
          <p className="LeftContainer__tasks-text" ref={textInput} contentEditable={isInput} onInput={changeValue}>{value}</p>
        </div>
        <button className="LeftContainer__tasks-btn" onClick={changeListOpen}>
          <span className="LeftContainer__tasks-circle"></span>
          <span className="LeftContainer__tasks-circle"></span>
          <span className="LeftContainer__tasks-circle"></span>
        </button>
        {isOpenList && <ChangeTask index={props.index} onChange={setIsOpenList} setInput={setIsInput} isInput={isInput} text={value}/>}
      </li>
    </>
  )
}

export default Task