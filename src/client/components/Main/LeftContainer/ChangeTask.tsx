//@ts-nocheck
import React, { useEffect, useRef } from "react";
import { Images } from "../../../assets/images/images";
import { changeTextTask, RootState } from "../../../store";
import { useSelector, useDispatch } from "react-redux";
import { changeNumberTask } from "../../../store";

interface ChangeTaskProps {
  index: number,
  onChange: any,
  setInput: any,
  isInput: boolean,
  text: any
}

export const ChangeTask = (props: ChangeTaskProps) => {
  const arrTasks = useSelector<RootState, Array<object>>(state => state.Tasks)
  const dispatch = useDispatch()

  const ref = useRef<HTMLDivElement>(null)

  useEffect(() => {
    let counter = 0
    function handleClick(event: MouseEvent) {
      if (event.target instanceof Node && ref.current?.contains(event.target)) {
      } else {
        if (counter != 0) props.onChange(false)
        else counter++
      }
    }
    document.addEventListener('click', handleClick)

    return () => {
      document.removeEventListener('click', handleClick)
    }
  }, [])

  function increase() {
    dispatch(changeNumberTask({
      index: arrTasks[props.index]['index'],
      number: arrTasks[props.index]['number'] + 1,
    }))
  }

  function decrease() {
    dispatch(changeNumberTask({
      index: arrTasks[props.index]['index'],
      number: arrTasks[props.index]['number'] - 1,
    }))
  }

  function changeText() {
    if (props.isInput) {
      props.setInput(false)
      console.log('Я тТУТ')
      console.log(props.text)
      dispatch(changeTextTask({
        index: arrTasks[props.index]['index'],
        text: props.text
      }))

    } else {
      props.setInput(true)
    }
    console.log(props.isInput)
  }

  return (
    <>

      <div className="LeftContainer__dropdown" ref={ref}>
        <ul className="LeftContainer__dropdown-list">
          <li className="LeftContainer__dropdown-item" onClick={increase}>
            <button className="LeftContainer__dropdown-btn">
              <img className="LeftContainer__dropdown-img" src={Images.plus.default} alt="" />
              <p className="LeftContainer__dropdown-text">Увеличить</p>
            </button>
          </li>
          <li className="LeftContainer__dropdown-item" onClick={decrease}>
            <button className="LeftContainer__dropdown-btn">
              <img className="LeftContainer__dropdown-img" src={Images.minus.default} alt="" />
              <p className="LeftContainer__dropdown-text">Уменьшить</p>
            </button>
          </li>
          <li className="LeftContainer__dropdown-item" onClick={changeText}>
            <button className="LeftContainer__dropdown-btn">
              <img className="LeftContainer__dropdown-img" src={Images.edit.default} alt="" />
              <p className="LeftContainer__dropdown-text">Редактировать</p>
            </button>
          </li>
          <li className="LeftContainer__dropdown-item">
            <button className="LeftContainer__dropdown-btn">
              <img className="LeftContainer__dropdown-img" src={Images.delete.default} alt="" />
              <p className="LeftContainer__dropdown-text">Удалить</p>
            </button>
          </li>
        </ul>
      </div>
    </>
  )
}

export default ChangeTask